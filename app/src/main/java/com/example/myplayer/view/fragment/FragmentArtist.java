package com.example.myplayer.view.fragment;


import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myplayer.R;
import com.example.myplayer.adapter.ArtistAdapter;
import com.example.myplayer.model.DetailManager;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.model.entity.Artist;
import com.example.myplayer.view.activity.ArtistDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentArtist extends Fragment implements FragmentArtistView {

    @BindView(R.id.rv_detail)
    RecyclerView artistRcv;

    @BindView(R.id.pb_detail)
    ProgressBar waitBar;

    private View v;
    private ArrayList<Artist> artists = new ArrayList<>();
    private ArrayList<Artist> coppyArtists = new ArrayList<>();
    private ArtistAdapter artistAdapter;
    private LoadArtist loadArtist;

    public FragmentArtist() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, v);
        setupRcv();
        getSong();

        return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void getSong() {
        loadArtist = new LoadArtist(artists, this);
        loadArtist.execute();

    }

    @Override
    public void loadDataFinish() {
        artistAdapter.notifyDataSetChanged();
        waitBar.setVisibility(View.GONE);
    }

    @Override
    public void loadDataStart() {
        waitBar.setVisibility(View.VISIBLE);
    }

    private void setupRcv() {
        artistRcv.setLayoutManager(new LinearLayoutManager(getActivity()));
        artistAdapter = new ArtistAdapter(getActivity(), artists, new ArtistAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                listSongofArtist(artists.get(posItem).getId());
            }
        });
        artistRcv.setAdapter(artistAdapter);
    }

    private class LoadArtist extends AsyncTask {

        private ArrayList<Artist> artists;
        private FragmentArtistView fragmentArtistView;

        public LoadArtist(ArrayList<Artist> artists, FragmentArtistView fragmentArtistView) {
            this.artists = artists;
            this.fragmentArtistView = fragmentArtistView;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ArrayList<Artist> mArtist = MainManager.getListArtist(getActivity());
            artists.clear();
            artists.addAll(mArtist);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            fragmentArtistView.loadDataFinish();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fragmentArtistView.loadDataStart();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            fragmentArtistView.loadDataFinish();
        }
    }

    private void listSongofArtist(int artistId) {
        ArtistDetailActivity.openArtistDetail(getActivity(),artistId);

    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loadArtist != null && loadArtist.getStatus() != AsyncTask.Status.FINISHED) {
            loadArtist.cancel(true);
        }
    }


    //<menu>--------------
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);
        MenuItem item = menu.findItem(R.id.item_detail_filter);
        SearchView searchView = (SearchView) item.getActionView();


        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                saveArtist();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                restoreArtist();
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updatefilterArtist(newText);

                return true;
            }
        });
    }

    private void updatefilterArtist(String newText) {
        ArrayList<Artist> filterSongs = DetailManager.filterArtists(coppyArtists,newText);
        artists.clear();
        artists.addAll(filterSongs);
        artistAdapter.notifyDataSetChanged();
    }

    private void restoreArtist() {
        artists.clear();
        artists.addAll(coppyArtists);
    }

    private void saveArtist() {
        coppyArtists.clear();
        coppyArtists.addAll(artists);
    }

    //</menu>--------------

}

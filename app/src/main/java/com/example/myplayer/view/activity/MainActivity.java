package com.example.myplayer.view.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myplayer.R;
import com.example.myplayer.adapter.CategoryAdapter;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.model.PlayManager;
import com.example.myplayer.model.entity.MainItem;
import com.example.myplayer.utils.SystemUtils;
import com.example.myplayer.view.receivers.NextBroadcastReceiver;
import com.example.myplayer.view.receivers.PausePlayBroadcastReceiver;
import com.example.myplayer.view.services.PlayMusicService;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar_main)
    Toolbar toolbar;
    @BindView(R.id.rcv_main)
    RecyclerView mainRecycleView;
    @BindView(R.id.ll_main_playingbar)
    LinearLayout mainBarLL;
    @BindView(R.id.tv_main_song_title)
    TextView titleSongTv;
    @BindView(R.id.tv_main_artist)
    TextView artistTv;
    @BindView(R.id.img_main_album)
    ImageView albumImg;
    @BindView(R.id.img_main_play_pause)
    ImageView playPauseImg;


    @BindView(R.id.img_wallpaper_main)
    ImageView wallpaperMainImg;

    private List<MainItem> mainList;
    private CategoryAdapter iAdapterName;
    private PlayMusicService curPlayMusicService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        PlayManager.getInstance().setMainActivity(this);

        setupToolBar();
        initDefaultWallpaper();
        checkPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, 100);
        initCurService();

        updateMainControlBar();
        //dang ky 1 receiver >> khi co su kien >> 1 doan code se dc thuc hien = interface remote
        registerBroadcastUpdatePlaying();

    }

    private void initCurService() {
        curPlayMusicService = PlayManager.getInstance().getPlayMusicService();

        if (curPlayMusicService != null) {
            mainBarLL.setVisibility(View.VISIBLE);
        } else {
            mainBarLL.setVisibility(View.GONE);
        }
    }

    // Function to check and request permission
    public void checkPermission(String permission, int requestCode) {
        // Checking if permission is not granted
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                permission) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(MainActivity.this,
                    new String[]{permission}, requestCode);
        } else {
            setupRecycleView();

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 100) {
            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                setupRecycleView();

            } else {
                Toast.makeText(MainActivity.this,
                        "Camera Permission Denied",
                        Toast.LENGTH_SHORT)
                        .show();
            }
        }
    }


    private void initDefaultWallpaper() {
        MainManager.setDefaultWallpaper(wallpaperMainImg, this);
    }

    private void setupRecycleView() {

        mainList = new ArrayList<>();
        int numSongs = MainManager.getListSong(this).size();
        int numAlbums = MainManager.getListAlbum(this).size();
        int numArtists = MainManager.getListArtist(this).size();

        mainList.add(new MainItem(R.drawable.ic_mm_song, getString(R.string.list_song), numSongs));
        mainList.add(new MainItem(R.drawable.ic_album_white, getString(R.string.album_list), numAlbums));
        mainList.add(new MainItem(R.drawable.ic_artist, getString(R.string.artist_list), numArtists));

        iAdapterName = new CategoryAdapter(this, mainList, new CategoryAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                DetailActivity.openDetailActivity(MainActivity.this, mainList.get(posItem).getTitle());
            }
        });

        mainRecycleView.setAdapter(iAdapterName);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        mainRecycleView.setLayoutManager(linearLayoutManager);

        mainRecycleView.setItemAnimator(new DefaultItemAnimator());

    }

    private void setupToolBar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        SystemUtils.setStatusBarTranslucent(true, this);

    }

    @OnClick({R.id.img_main_play_pause,
            R.id.ll_main_playingbar,
            R.id.img_main_next,})
    public void onclick(View v) {
        switch (v.getId()) {
            case R.id.img_main_play_pause:
                playPauseMainBar();
                break;
            case R.id.img_main_next:
                nextMainbar();
                break;
            case R.id.ll_main_playingbar:
                PlaySongActivity.openPlayActivityFromMainControlBar(this);
                break;


        }
    }

    private void nextMainbar() {
        if (curPlayMusicService != null) {
            makeServiceNextSong();
            playPauseImg.setImageResource(R.drawable.pb_pause);
        }
    }

    private void playPauseMainBar() {
        if (curPlayMusicService != null) {
            makeServicePauPlaySong();
        }
    }

    public void changePlayButtonStateStop() {
        playPauseImg.setImageResource(R.drawable.pb_play);
    }

    public void updateMusicBar() {
        titleSongTv.setText(curPlayMusicService.getCurrentSong().getTitle());
        artistTv.setText(curPlayMusicService.getCurrentSong().getArtist());

        if (curPlayMusicService.isPlaying()) {
            playPauseImg.setImageResource(R.drawable.pb_pause);
        } else {
            playPauseImg.setImageResource(R.drawable.pb_play);
        }
        String albumPath = curPlayMusicService.getCurrentSong().getAlbumImagePath();
        if (albumPath != null) {
            Bitmap bitmap = BitmapFactory.decodeFile(albumPath);
            albumImg.setImageBitmap(bitmap);
        } else {
            albumImg.setImageResource(R.drawable.default_cover);
        }
    }

    private void makeServiceNextSong() {
        Intent intent = new Intent(getApplicationContext(), NextBroadcastReceiver.class);
        sendBroadcast(intent);
    }

    private void makeServicePauPlaySong() {
        Intent intent = new Intent(getApplicationContext(), PausePlayBroadcastReceiver.class);
        sendBroadcast(intent);
    }

    private void registerBroadcastUpdatePlaying() {
        IntentFilter intentFilter = new IntentFilter(PlayMusicService.ACTION_UPDATE_PlAY_STATUS);
        registerReceiver(broadcastReceiverUpdatePlaying, intentFilter);
    }

    private void unRegisterBroadcastUpdatePlaying() {
        unregisterReceiver(broadcastReceiverUpdatePlaying);
    }

    BroadcastReceiver broadcastReceiverUpdatePlaying = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            updateMainControlBar();
        }
    };

    private void updateMainControlBar() {
        curPlayMusicService = PlayManager.getInstance().getPlayMusicService();
        if (curPlayMusicService != null) {
            mainBarLL.setVisibility(View.VISIBLE);
            updateMusicBar();
            if (curPlayMusicService.isPlaying()) {
                playPauseImg.setImageResource(R.drawable.pb_pause);
            } else {
                playPauseImg.setImageResource(R.drawable.pb_play);
            }
        } else {
            mainBarLL.setVisibility(View.GONE);
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        PlayManager.getInstance().setMainActivity(null);
        unRegisterBroadcastUpdatePlaying();
    }

}

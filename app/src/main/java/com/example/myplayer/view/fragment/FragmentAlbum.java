package com.example.myplayer.view.fragment;


import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myplayer.R;
import com.example.myplayer.adapter.AlbumAdapter;
import com.example.myplayer.model.DetailManager;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.model.entity.Album;
import com.example.myplayer.view.activity.AlbumDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentAlbum extends Fragment {

    @BindView(R.id.rv_detail)
    RecyclerView albumRcv;

    @BindView(R.id.pb_detail)
    ProgressBar progressBar;
    private View v;
    private ArrayList<Album> albums=new ArrayList<>();
    private ArrayList<Album> coppyAlbums=new ArrayList<>();
    private AlbumAdapter albumAdapter;
    private LoadAlbum loadAlbum;

    public FragmentAlbum() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, v);
        setupRcv();
        getALbum();

        return v;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);

    }

    private void getALbum() {
        loadAlbum = new LoadAlbum(albums, new FragmentAlbumView() {
            @Override
            public void loadDataFinish() {
                albumAdapter.notifyDataSetChanged();
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void loadDataStart() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });

        loadAlbum.execute();
    }

    private void setupRcv() {
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(),3);
        albumRcv.setLayoutManager(gridLayoutManager);

        albumAdapter = new AlbumAdapter(getContext(), albums, new AlbumAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                listSongsOfAlbum(albums.get(posItem).getId());
            }
        });
        albumRcv.setAdapter(albumAdapter);

    }

    private void listSongsOfAlbum(int albumId) {
        AlbumDetailActivity.openAlbumDetail(getActivity(),albumId);
    }

    private class LoadAlbum extends AsyncTask {

        private ArrayList<Album> albums;
        private FragmentAlbumView fragmentAlbumView;

        public LoadAlbum(ArrayList<Album> albums, FragmentAlbumView fragmentAlbumView) {
            this.albums = albums;
            this.fragmentAlbumView = fragmentAlbumView;
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ArrayList<Album> mArtist = MainManager.getListAlbum(getActivity());
            albums.clear();
            albums.addAll(mArtist);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            fragmentAlbumView.loadDataFinish();

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fragmentAlbumView.loadDataStart();

        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            fragmentAlbumView.loadDataFinish();
        }
    }
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loadAlbum != null && loadAlbum.getStatus() != AsyncTask.Status.FINISHED) {
            loadAlbum.cancel(true);
        }
    }


    //<menu>--------------
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);
        MenuItem item = menu.findItem(R.id.item_detail_filter);
        SearchView searchView = (SearchView) item.getActionView();


        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                saveAlbum();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                restoreAlbum();
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                updatefilterAlbum(newText);

                return true;
            }
        });
    }

    private void updatefilterAlbum(String newText) {
        ArrayList<Album> filterSongs = DetailManager.filterAlbums(coppyAlbums,newText);
        albums.clear();
        albums.addAll(filterSongs);
        albumAdapter.notifyDataSetChanged();
    }

    private void restoreAlbum() {
        albums.clear();
        albums.addAll(coppyAlbums);
    }

    private void saveAlbum() {
        coppyAlbums.clear();
        coppyAlbums.addAll(albums);
    }

    //</menu>--------------

}

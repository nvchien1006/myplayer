package com.example.myplayer.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myplayer.R;
import com.example.myplayer.adapter.SongAdapter;
import com.example.myplayer.model.DetailManager;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.model.entity.Song;
import com.example.myplayer.utils.SystemUtils;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

//IN
// KEY_ARTIST

public class ArtistDetailActivity extends AppCompatActivity {

    private static final String KEY_ARTIST = "key_arstist";
    @BindView(R.id.tv_title)
    TextView title;

    @BindView(R.id.toolbar_categorydetail_detail)
    Toolbar toolbar;

    @BindView(R.id.img_categorydetail_bg)
    ImageView categorydetailBgIv;

    @BindView(R.id.img_categorydetail_thumb)
    ImageView categorydetailThumbIv;

    @BindView(R.id.rcv_category_detail)
    RecyclerView categoryDetailRcv;

    private ArrayList<Song> songs = new ArrayList<>();
    private SongAdapter songAdapter;

    public static void openArtistDetail(Context context, int AdtisrtId) {
        Intent intent = new Intent(context, ArtistDetailActivity.class);
        intent.putExtra(KEY_ARTIST, AdtisrtId);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_detail);
        ButterKnife.bind(this);

        setupToolbar();
        setupAlbumDetailRcv();
        MainManager.setDefaultWallpaper(categorydetailBgIv, this);
        updateSongs();

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void setUpthumb() {
        if (songs.size() > 0) {

            String path = songs.get(0).getAlbumImagePath();
            if (path != null) {
                File file = new File(path);
                Uri uri = Uri.fromFile(file);
                categorydetailThumbIv.setImageURI(uri);
            }
            title.setText(songs.get(0).getAlbum());
        }

    }


    public void updateSongs() {
        Intent intent = getIntent();
        int mArtistId = intent.getExtras().getInt(KEY_ARTIST);
        ArrayList<Song> mListSong = DetailManager.getListSongOfArtist(mArtistId, this);
        songs.clear();
        songs.addAll(mListSong);
        songAdapter.notifyDataSetChanged();
        setUpthumb();
    }


    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        SystemUtils.setStatusBarTranslucent(true, this);
    }

    private void setupAlbumDetailRcv() {
        categoryDetailRcv.setLayoutManager(new LinearLayoutManager(this));
        songAdapter = new SongAdapter(this, songs, new SongAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                //
            }
        });
        categoryDetailRcv.setAdapter(songAdapter);
    }
}

package com.example.myplayer.view.fragment;

public interface FragmentSongListView {
    void loadDataFinish();

    void loadDataStart();
}

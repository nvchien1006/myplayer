package com.example.myplayer.view.fragment;

public interface FragmentAlbumView {
    void loadDataFinish();
    void loadDataStart();
}

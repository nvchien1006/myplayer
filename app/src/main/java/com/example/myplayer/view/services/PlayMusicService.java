package com.example.myplayer.view.services;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.widget.RemoteViews;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.example.myplayer.Constants.Bundles;
import com.example.myplayer.Constants.Receivers;
import com.example.myplayer.R;
import com.example.myplayer.model.PlayManager;
import com.example.myplayer.model.entity.Song;
import com.example.myplayer.view.activity.MainActivity;
import com.example.myplayer.view.activity.PlaySongActivity;
import com.example.myplayer.view.receivers.NextBroadcastReceiver;
import com.example.myplayer.view.receivers.PausePlayBroadcastReceiver;
import com.example.myplayer.view.receivers.PreBroadcastReceiver;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class PlayMusicService extends Service {

    private static final String CHANNEL_ID = "channel_play";
    private static final int NOTIFICATION_ID = 111;

    public static final String ACTION_UPDATE_PlAY_STATUS ="ACTION_UPDATE_PlAY_STATUS";
    private static final String ACTION_STOP_SERVICE = "ACTION_STOP_SERVICE";
    public static final String ACTION_COMPLETE_SONG = "ACTION_COMPLETE_SONG";
    public static final String ACTION_SWITCH_SONG = "ACTION_COMPLETE_SONG";
    public static final String KEY_ID_SWITH = "KEY_ID_SWITH";

    private boolean isRepeat = false;
    private boolean isShuffle = false;

    private static MediaPlayer mediaPlayer;

    private final IBinder mBinder = new LocalBinder();
    private Random rand;

    private ArrayList<Song> playingSongList;
    private int currentSongPos;
    private String albumArtPath;
    private Song currentSong;

    private RemoteViews bigViews;
    private RemoteViews views;
    private Notification notification;
    private boolean isShowNotification = false;

    public PlayMusicService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (ACTION_STOP_SERVICE.equals(intent.getAction())) {

            PlaySongActivity playSongActivity = PlayManager.getInstance().getPlaySongActivity();
            if (playSongActivity != null) {
                playSongActivity.changePlayButtonStateStop();
            }

            MainActivity mainActivity =PlayManager.getInstance().getMainActivity();
            if(mainActivity != null){
                mainActivity.changePlayButtonStateStop();
            }
//
            if (playSongActivity == null && mainActivity == null) {
                stopSelf();
            }

            pauseMusic();
            stopForeground(true);
            isShowNotification = false;

        } else {
            showNotification(isShowNotification());
            isShowNotification = true;
        }
        return START_NOT_STICKY;
    }

    public void seekTo(int progress) {
        mediaPlayer.seekTo(progress*1000);
    }

    public class LocalBinder extends Binder {

        public PlayMusicService getInstantBoundService() {
            return PlayMusicService.this;
        }

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        rand = new Random();
    }


    //-------------------------------
    public void playMusic(String path) {
        releaseMusic();
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (PlayManager.getInstance().getPlaySongActivity() != null) {
                    Intent intent = new Intent(ACTION_COMPLETE_SONG);
                    sendBroadcast(intent);
                    showNotification(true);
                } else {
                    if (isRepeat()) {
                        playMusic(currentSong.getPath());
                    } else {
                        openNextMusic();
                    }
                }
            }
        });

        mediaPlayer.start();
    }

    public void openNextMusic() {
        currentSongPos = getNextPosition();
        openCurMusic();
    }

    public void openPreMusic() {
        currentSongPos = getPrePosition();
        openCurMusic();

    }

    private void updateHomeBar() {
        MainActivity mainActivity = PlayManager.getInstance().getMainActivity();
        if(mainActivity != null){
            mainActivity.updateMusicBar();
        }
    }

    private void openCurMusic() {
        currentSong = playingSongList.get(currentSongPos);
        albumArtPath = currentSong.getAlbumImagePath();
        String path = currentSong.getPath();
        if (PlayManager.getInstance().getPlaySongActivity() != null) {
            setAlbumArt();
        }
        playMusic(path);
        showNotification(true);
        updateHomeBar();

    }


    private void setAlbumArt() {

        Intent intent1 = new Intent(Receivers.ACTION_CHANGE_ALBUM_ART);
        intent1.putExtra(Bundles.KEY_ALBUM_PLAY, playingSongList.get(currentSongPos).getAlbumImagePath());
        sendBroadcast(intent1);

    }

    private void releaseMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
            mediaPlayer.release();
        }
    }

    public void stopMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
    }

    public int getTotalTime() {
        return mediaPlayer.getDuration() / 1000;
    }

    public int getCurrentLength() {
        return mediaPlayer.getCurrentPosition() / 1000;
    }

    public boolean isPlaying() {
        return mediaPlayer.isPlaying();
    }

    public void pauseMusic() {
        if (mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            changePlayPauseNotifyState();
        }
    }

    public void resumeMusic() {
        if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
            mediaPlayer.start();
            changePlayPauseNotifyState();
        }
    }

    public void changePlayPauseNotifyState() {

        if (isPlaying()) {
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
        } else {
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
        }
        startForeground(NOTIFICATION_ID, notification);

    }


    public void setRepeat(boolean repeat) {
        this.isRepeat = repeat;
    }

    public boolean isRepeat() {
        return isRepeat;
    }

    public int getPrePosition() {
        int newSongPosition = currentSongPos;
        if (isShuffle) { //ngau nhien
            while (newSongPosition == currentSongPos) {
                newSongPosition = rand.nextInt(playingSongList.size());
            }
            return newSongPosition;
        }else { //tuan tu
            if (currentSongPos == 0) {
                currentSongPos = playingSongList.size() - 1;
            } else {
                currentSongPos--;
            }
            newSongPosition = currentSongPos;
            return newSongPosition;
        }
    }

    public int getNextPosition() {
        if (isShuffle) {
            int newSongPosition = currentSongPos;
            while (newSongPosition == currentSongPos)
                newSongPosition = rand.nextInt(playingSongList.size());
            return newSongPosition;
        } else {
            if (isEnd()) {
                return 0;
            } else {
                currentSongPos = currentSongPos + 1;
                return currentSongPos;
            }
        }

    }

    private boolean isEnd() {
        return currentSongPos == playingSongList.size() - 1;
    }

    public void setDataForNotification(ArrayList<Song> lstSong, int currentPos,
                                       Song itemCurrent, String albumArtPath) {
        this.playingSongList = lstSong;
        this.currentSongPos = currentPos;
        this.albumArtPath = albumArtPath;
        this.currentSong = itemCurrent;

//        showLockScreen();
    }

    public boolean isShuffle() {
        return isShuffle;
    }

    public void setShuffle(boolean shuffle) {
        isShuffle = shuffle;
    }

    public boolean isShowNotification() {
        return isShowNotification;
    }

    public void setShowNotification(boolean showNotification) {
        isShowNotification = showNotification;
    }

    public Notification showNotification(boolean update) {

        bigViews = new RemoteViews(getPackageName(), R.layout.notification_view_expanded);
        views = new RemoteViews(getPackageName(), R.layout.notification_view);

        Intent intent = new Intent(getApplicationContext(), PlaySongActivity.class);
        intent.putExtra(PlaySongActivity.IS_PLAY, true);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.mipmap.ic_launcher);
        builder.setContentIntent(pendingIntent);
        builder.setContent(views);
        builder.setCustomBigContentView(bigViews);

        createNotificationChannel();

        if (isPlaying()) {
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_pause);
        } else {
            views.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
            bigViews.setImageViewResource(R.id.btn_play_pause_noti, R.drawable.pb_play);
        }

        bigViews.setTextViewText(R.id.tv_song_title_noti, currentSong.getTitle());
        bigViews.setTextViewText(R.id.tv_artist_noti, currentSong.getArtist());

        views.setTextViewText(R.id.tv_song_title_noti, currentSong.getTitle());
        views.setTextViewText(R.id.tv_artist_noti, currentSong.getArtist());

        if (albumArtPath != null && !albumArtPath.isEmpty()) {
            Bitmap bitmap = BitmapFactory.decodeFile(albumArtPath);
            bigViews.setImageViewBitmap(R.id.img_album_art_noti, bitmap);
            views.setImageViewBitmap(R.id.img_album_art_noti, bitmap);
        } else {
            bigViews.setImageViewResource(R.id.img_album_art_noti, R.drawable.adele);
            views.setImageViewResource(R.id.img_album_art_noti, R.drawable.adele);
        }

        Intent intentPrev = new Intent(getApplicationContext(), PreBroadcastReceiver.class);
        intentPrev.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentPrev = PendingIntent.getBroadcast(getApplicationContext(),
                0, intentPrev, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentPlayPause = new Intent(getApplicationContext(), PausePlayBroadcastReceiver.class);
        intentPlayPause.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentPlayPause = PendingIntent.getBroadcast(getApplicationContext(),
                0, intentPlayPause, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentNext = new Intent(getApplicationContext(), NextBroadcastReceiver.class);
        intentNext.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntentNext = PendingIntent.getBroadcast(getApplicationContext(),
                0, intentNext, PendingIntent.FLAG_UPDATE_CURRENT);

        Intent intentStopSelf = new Intent(this, PlayMusicService.class);
        intentStopSelf.setAction(ACTION_STOP_SERVICE);
        PendingIntent pendingIntentStopSelf = PendingIntent.getService(this, 0, intentStopSelf, PendingIntent.FLAG_UPDATE_CURRENT);

        bigViews.setOnClickPendingIntent(R.id.btn_close_noti, pendingIntentStopSelf);
        bigViews.setOnClickPendingIntent(R.id.btn_prev_noti, pendingIntentPrev);
        bigViews.setOnClickPendingIntent(R.id.btn_next_noti, pendingIntentNext);
        bigViews.setOnClickPendingIntent(R.id.btn_play_pause_noti, pendingIntentPlayPause);

        views.setOnClickPendingIntent(R.id.btn_close_noti, pendingIntentStopSelf);
        views.setOnClickPendingIntent(R.id.btn_next_noti, pendingIntentNext);
        views.setOnClickPendingIntent(R.id.btn_play_pause_noti, pendingIntentPlayPause);

        notification = builder.build();

        if (update) {
            startForeground(NOTIFICATION_ID, notification);
        }
        return notification;

    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(channel);
            }
        }
    }

    public ArrayList<Song> getPlayingSongList() {
        return playingSongList;
    }

    public void setPlayingSongList(ArrayList<Song> playingSongList) {
        this.playingSongList = playingSongList;
    }

    public int getCurrentSongPos() {
        return currentSongPos;
    }

    public void setCurrentSongPos(int currentSongPos) {
        this.currentSongPos = currentSongPos;
    }

    public String getAlbumArtPath() {
        return albumArtPath;
    }

    public void setAlbumArtPath(String albumArtPath) {
        this.albumArtPath = albumArtPath;
    }

    public Song getCurrentSong() {
        return currentSong;
    }

    public void setCurrentSong(Song currentSong) {
        this.currentSong = currentSong;
    }

    public void playPausePlayer() {
        if (mediaPlayer.isPlaying()) {
            pauseMusic();
        } else {
            resumeMusic();
        }
        updateHomeBar();

    }

}

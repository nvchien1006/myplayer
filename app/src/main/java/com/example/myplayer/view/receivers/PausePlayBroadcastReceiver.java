package com.example.myplayer.view.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.myplayer.model.PlayManager;
import com.example.myplayer.view.activity.PlaySongActivity;
import com.example.myplayer.view.services.PlayMusicService;

public class PausePlayBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PlaySongActivity playSongActivity = PlayManager.getInstance().getPlaySongActivity();
        PlayMusicService playMusicService = PlayManager.getInstance().getPlayMusicService();

        if (playSongActivity != null) {
            playSongActivity.playPausePlayer();
        }else {
            playMusicService.playPausePlayer();
        }
    }
}

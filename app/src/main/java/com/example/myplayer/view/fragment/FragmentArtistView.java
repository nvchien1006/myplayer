package com.example.myplayer.view.fragment;

public interface FragmentArtistView {
    void loadDataFinish();

    void loadDataStart();
}

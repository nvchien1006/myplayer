package com.example.myplayer.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.example.myplayer.R;
import com.example.myplayer.adapter.ViewPagerDetailAdapter;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.utils.SystemUtils;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

//IN
//BUNDLE_CURTYPE

public class DetailActivity extends AppCompatActivity {

    private static final String BUNDLE_CURTYPE = "BUNDLE_CURTYPE";

    private ViewPagerDetailAdapter mVPAdapter;


    @BindView(R.id.img_back_ground_detail)
    ImageView backgroundDetailImg;
    @BindView(R.id.tablayout_detail)
    TabLayout tabLayout;
    @BindView(R.id.viewpager_detail)
    ViewPager viewpager;
    private String idType;


    public static void  openDetailActivity(Context context, String title) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(BUNDLE_CURTYPE,title);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);

        setUpToolbar();
        initDefaultWallpaper();
        setUpViewPageVsTab();
        getType();
    }

    private void getType() {
        Intent intent = getIntent();
        idType = intent.getStringExtra(BUNDLE_CURTYPE);

        if (idType.equals(getString(R.string.list_song))) {
            viewpager.setCurrentItem(0);
        } else if (idType.equals(getString(R.string.album_list))) {
            viewpager.setCurrentItem(1);
        } else if (idType.equals(getString(R.string.artist_list))) {
            viewpager.setCurrentItem(2);
        }
    }
    private void setUpViewPageVsTab() {
        mVPAdapter = new ViewPagerDetailAdapter(getSupportFragmentManager(), this);
        viewpager.setAdapter(mVPAdapter);
        tabLayout.setupWithViewPager(viewpager);
    }

    private void initDefaultWallpaper() {
        MainManager.setDefaultWallpaper(backgroundDetailImg,this);
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_detail);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        SystemUtils.setStatusBarTranslucent(true,this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);

    }
}

package com.example.myplayer.view.fragment;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myplayer.R;
import com.example.myplayer.adapter.SongAdapter;
import com.example.myplayer.model.entity.Song;
import com.example.myplayer.view.activity.PlaySongActivity;

import java.util.ArrayList;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

//IN >>
public class FragmentPlayList extends Fragment {
    View view;
    public static final String KEY_PLAY_LIST = "key_play_list";
    public ArrayList<Song> songs = new ArrayList<>();

    @BindView(R.id.rcv_playlist)
    RecyclerView rcv_playlist;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            ArrayList<Song> songsInput = (ArrayList<Song>) getArguments().getSerializable(KEY_PLAY_LIST);
            if (songsInput != null) {
                songs.addAll(songsInput);
            }
        }
    }

    public static FragmentPlayList newInstance(ArrayList<Song> mData) {
        FragmentPlayList fragment = new FragmentPlayList();
        Bundle args = new Bundle();
        args.putSerializable(KEY_PLAY_LIST, mData);
        fragment.setArguments(args);
        return fragment;
    }

    public FragmentPlayList() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_fragment_play_list, container, false);
        ButterKnife.bind(this, view);

        setupRecycleView();
        return view;
    }

    private void setupRecycleView() {
        rcv_playlist.setLayoutManager(new LinearLayoutManager(getContext()));
        SongAdapter songAdapter = new SongAdapter(getContext(), songs, new SongAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                openSongIn(posItem);
            }
        });
        rcv_playlist.setAdapter(songAdapter);
    }

    private void openSongIn(int posItem) {
        ((PlaySongActivity) Objects.requireNonNull(getActivity())).playSongIn(posItem);
    }

}

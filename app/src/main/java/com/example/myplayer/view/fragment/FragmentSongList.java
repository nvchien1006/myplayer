package com.example.myplayer.view.fragment;

import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.SearchView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.myplayer.R;
import com.example.myplayer.adapter.SongAdapter;
import com.example.myplayer.model.DetailManager;
import com.example.myplayer.model.MainManager;
import com.example.myplayer.model.entity.Song;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentSongList extends Fragment implements FragmentSongListView {

    @BindView(R.id.rv_detail)
    RecyclerView songRcv;
    @BindView(R.id.pb_detail)
    ProgressBar waitBar;

    private View v;
    private ArrayList<Song> songs = new ArrayList<>();
    private ArrayList<Song> coppySongs = new ArrayList<>();
    private SongAdapter songAdapter;
    private LoadListSong loadListSong;

    public FragmentSongList() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        v = inflater.inflate(R.layout.fragment_detail, container, false);
        ButterKnife.bind(this, v);
        setupRcv();
        getSong();

        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setHasOptionsMenu(true);

    }

    private void getSong() {
        loadListSong = new LoadListSong(songs, this);
        loadListSong.execute();

    }

    @Override
    public void loadDataFinish() {
        songAdapter.notifyDataSetChanged();
        waitBar.setVisibility(View.GONE);
    }

    @Override
    public void loadDataStart() {
        waitBar.setVisibility(View.VISIBLE);
    }

    private void setupRcv() {
        songRcv.setLayoutManager(new LinearLayoutManager(getActivity()));
        songAdapter = new SongAdapter(getActivity(), songs, new SongAdapter.ClickItemRcv() {
            @Override
            public void onClickItemRcv(int posItem) {
                playSong();
            }
        });
        songRcv.setAdapter(songAdapter);
    }

    private class LoadListSong extends AsyncTask {

        private ArrayList<Song> songs;
        private FragmentSongListView fragmentSongListView;

        public LoadListSong(ArrayList<Song> songs, FragmentSongListView fragmentSongListView) {
            this.songs = songs;
            this.fragmentSongListView = fragmentSongListView;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            fragmentSongListView.loadDataStart();
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            fragmentSongListView.loadDataFinish();
        }

        @Override
        protected Object doInBackground(Object[] params) {
            ArrayList<Song> mListSong = MainManager.getListSong(getActivity());
            songs.clear();
            songs.addAll(mListSong);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            fragmentSongListView.loadDataFinish();
        }
    }

    private void playSong() {

    }

    //<menu>--------------
    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_detail, menu);
        MenuItem item = menu.findItem(R.id.item_detail_filter);
        SearchView searchView = (SearchView) item.getActionView();


        item.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                saveSong();
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                restoreSong();
                return true;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ArrayList<Song> filterSongs = DetailManager.filterSong(coppySongs,newText);
                songs.clear();
                songs.addAll(filterSongs);
                songAdapter.notifyDataSetChanged();

                return true;
            }
        });
    }

    private void restoreSong() {
        songs.clear();
        songs.addAll(coppySongs);
    }

    private void saveSong() {
        coppySongs.clear();
        coppySongs.addAll(songs);
    }

    //</menu>--------------

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (loadListSong != null && loadListSong.getStatus() != AsyncTask.Status.FINISHED) {
            loadListSong.cancel(true);
        }
    }


}

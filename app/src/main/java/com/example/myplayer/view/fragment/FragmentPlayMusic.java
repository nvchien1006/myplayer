package com.example.myplayer.view.fragment;


import android.animation.ObjectAnimator;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.example.myplayer.Constants.Bundles;
import com.example.myplayer.Constants.Receivers;
import com.example.myplayer.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;


public class FragmentPlayMusic extends Fragment {
    //in
    public static final String KEY_PLAY = "key_play";
    private View v;
    private String path;

    @BindView(R.id.img_playmusic)
    CircleImageView thumbCircleImageView;
    private ObjectAnimator anim;

    public FragmentPlayMusic() {
    }

    public static FragmentPlayMusic newInstance(String imgPath) {
        FragmentPlayMusic fragment = new FragmentPlayMusic();
        Bundle args = new Bundle();
        args.putString(KEY_PLAY, imgPath);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        registerBroadcastAlbumArt();
        if (getArguments() != null) {
            path = getArguments().getString(KEY_PLAY);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_fragment_play_music, container, false);
        ButterKnife.bind(this, v);
        showThumb(path);
        setAnimationThumb();
        return v;
    }

    private void setAnimationThumb() {
        thumbCircleImageView.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        anim = ObjectAnimator.ofFloat(thumbCircleImageView, View.ROTATION, 0f, 360f)
                .setDuration(5000);
        anim.setRepeatCount(Animation.INFINITE);
        anim.setInterpolator(new LinearInterpolator());
        anim.start();

    }

    public void showThumb(String path) {
        if (path!= null) {
            Bitmap bitmap = BitmapFactory.decodeFile(path);
            thumbCircleImageView.setImageBitmap(bitmap);
        } else {
            thumbCircleImageView.setImageResource(R.drawable.default_cover_big);
        }
    }

    public void pauseAnimateAlbumThumb() {
        if (anim.isRunning()) {
            anim.pause();
        }
    }

    public void resumeAnnimateAlbumThumb() {
        if (anim.isPaused()) {
            anim.resume();
        }
    }

    public void startAnnimateAlbumThumb() {
        if(anim!=null) {
            anim.start();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterBroadcastAlbumArt();
    }

    BroadcastReceiver chanceAlbumBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            path = intent.getExtras().getString(Bundles.KEY_ALBUM_PLAY);
            showThumb(path);
        }
    };

    private void registerBroadcastAlbumArt() {
        IntentFilter filter = new IntentFilter(Receivers.ACTION_CHANGE_ALBUM_ART);
        getActivity().registerReceiver(chanceAlbumBroadcastReceiver, filter);
    }



    private void unregisterBroadcastAlbumArt() {
        getActivity().unregisterReceiver(chanceAlbumBroadcastReceiver);
    }


}

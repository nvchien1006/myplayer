package com.example.myplayer.view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.viewpager.widget.ViewPager;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myplayer.Constants.Receivers;
import com.example.myplayer.R;
import com.example.myplayer.adapter.ViewPagerPlayAdapter;
import com.example.myplayer.model.PlayManager;
import com.example.myplayer.model.entity.Song;
import com.example.myplayer.utils.BlurBuilder;
import com.example.myplayer.utils.Common;
import com.example.myplayer.view.services.PlayMusicService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.example.myplayer.Constants.Bundles.KEY_ALBUM_PLAY;
import static com.example.myplayer.utils.SystemUtils.setStatusBarTranslucent;
import static com.example.myplayer.view.services.PlayMusicService.ACTION_SWITCH_SONG;
import static com.example.myplayer.view.services.PlayMusicService.ACTION_UPDATE_PlAY_STATUS;

public class PlaySongActivity extends AppCompatActivity {

    public static final String IS_PLAY = "is_play";
    public static final String SONG_PATH = "song_path";
    public static final String LIST_SONG = "list_song";
    public static final String SONG_POS = "position";

    @BindView(R.id.view_pager_play)
    ViewPager viewPager;
    @BindView(R.id.toolbar_playing)
    Toolbar toolbar;
    @BindView(R.id.rl_control)
    ViewGroup controlViewGroup;
    @BindView(R.id.tv_time_played)
    TextView curTimeTv;
    @BindView(R.id.seek_bar_play)
    SeekBar seekbarPlay;
    @BindView(R.id.tv_time_total)
    TextView totalTimeTv;

    @BindView(R.id.tv_song_name_play)
    TextView songNameTv;
    @BindView(R.id.tv_artist_play)
    TextView artistNameTv;

    @BindView(R.id.img_shuffle)
    ImageView shuffleImg;
    @BindView(R.id.img_prev)
    ImageView preImageView;
    @BindView(R.id.img_play_pause)
    ImageView playPauseImg;
    @BindView(R.id.img_next)
    ImageView nextImg;
    @BindView(R.id.img_repeat)
    ImageView repeatImg;
    @BindView(R.id.activity_play_music)
    ViewGroup bgViewGroup;

    private ArrayList<Song> songs = new ArrayList<>();
    private ViewPagerPlayAdapter viewPagerPlayAdapter;
    private String curSongPath;
    private int currentSongPos;
    private ArrayList<Song> curSongList;
    private PlayMusicService mPlayMusicService;
    private boolean isSeeking = false;
    private boolean isShuffle =false;

    public static void openPlayActivityFromMainControlBar(MainActivity mainActivity) {
        Intent intent = new Intent(mainActivity, PlaySongActivity.class);
        intent.putExtra(PlaySongActivity.IS_PLAY, true);
        mainActivity.startActivity(intent);
        mainActivity.overridePendingTransition(R.anim.slide_in_up,R.anim.no_change);
    }

    public static void openPlayActivity(Context context, int id, boolean isPlay, ArrayList<Song> playList) {
        Intent intent = new Intent(context, PlaySongActivity.class);
        intent.putExtra(SONG_PATH, playList.get(id).getPath());
        intent.putExtra(SONG_POS, id);
        intent.putExtra(LIST_SONG, playList);
        intent.putExtra(IS_PLAY, isPlay);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_song);
        ButterKnife.bind(this);

        PlayManager.getInstance().setPlaySongActivity(this);
        mPlayMusicService = (PlayMusicService) PlayManager.getInstance().getPlayMusicService();

        getDataFromIntent();
        setupViewPage();
        setUpToolbar();
        setupSeekBar();

        openPlayServiceInBackground();

        setUpRegisterBroadCast();
        setUpHomePlayControl();
    }

    private void setUpHomePlayControl() {

    }

    private void setUpRegisterBroadCast() {

        registerBroadcastSongComplete();
//        registerBroadcastSwitchSong();
    }

    @OnClick({R.id.img_shuffle,
            R.id.img_repeat,
            R.id.img_next,
            R.id.img_prev,
            R.id.img_play_pause})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_play_pause:
                playPausePlayer();
                break;
            case R.id.img_repeat:
                repeat();
                break;
            case R.id.img_shuffle:
                shuffle();
                break;
            case R.id.img_prev:
                prevPlayer();
                break;
            case R.id.img_next:
                nextPlayer();
                break;
        }
    }

    public void changePlayButtonStateStop() {
        playPauseImg.setImageResource(R.drawable.pb_play);
        viewPagerPlayAdapter.fragmentPlayMusic.pauseAnimateAlbumThumb();

    }


    public void nextPlayer() {
        if (!mPlayMusicService.isRepeat()) {
            currentSongPos = mPlayMusicService.getNextPosition();
            curSongPath = curSongList.get(currentSongPos).getPath();
        }
        playMusic();
    }

    public void playSongIn(int position){
        currentSongPos = position;
        curSongPath = curSongList.get(position).getPath();
        playMusic();
    }

    public void prevPlayer() {
        if (!mPlayMusicService.isRepeat()) {
            currentSongPos = mPlayMusicService.getPrePosition();
            curSongPath = curSongList.get(currentSongPos).getPath();
        }
        playMusic();
    }

    private void shuffle() {
        if (mPlayMusicService == null) return;
        if (mPlayMusicService.isShuffle()) {
            shuffleImg.setImageResource(R.drawable.ic_widget_shuffle_off);
            mPlayMusicService.setShuffle(false);
        } else {
            shuffleImg.setImageResource(R.drawable.ic_widget_shuffle_on);
            mPlayMusicService.setShuffle(true);
        }
    }

    private void repeat() {
        if (mPlayMusicService.isRepeat()) {
            repeatImg.setImageResource(R.drawable.ic_widget_repeat_off);
            mPlayMusicService.setRepeat(false);
        } else {
            repeatImg.setImageResource(R.drawable.ic_widget_repeat_one);
            mPlayMusicService.setRepeat(true);
        }
    }

    public void playPausePlayer() {
        if (mPlayMusicService.isPlaying()) {
            playPauseImg.setImageResource(R.drawable.pb_play);
            viewPagerPlayAdapter.fragmentPlayMusic.pauseAnimateAlbumThumb();
            mPlayMusicService.pauseMusic();
        } else {
            playPauseImg.setImageResource(R.drawable.pb_pause);
            viewPagerPlayAdapter.fragmentPlayMusic.resumeAnnimateAlbumThumb();
            mPlayMusicService.resumeMusic();
        }
        updateHomeActivity();
    }

    private void setupSeekBar() {
        seekbarPlay.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                curTimeTv.setText(Common.miliSecondToString(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                isSeeking = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPlayMusicService.seekTo(seekBar.getProgress());
                if (!mPlayMusicService.isPlaying()) {
                    mPlayMusicService.resumeMusic();
                    playPauseImg.setImageResource(R.drawable.pb_pause);
                }
                isSeeking = false;
                updateSeekBar();
            }
        });
    }


    private void openPlayServiceInBackground() {
        mPlayMusicService = PlayManager.getInstance().getPlayMusicService();
        if (mPlayMusicService == null) {// th new
            Intent intent = new Intent(this, PlayMusicService.class);
            bindService(intent, serviceConnection, BIND_AUTO_CREATE);
        }else {// th old
            updateSeekBar();
            updateTitle();
            updatePlayControl();
            mPlayMusicService.showNotification(!mPlayMusicService.isShowNotification());


            if (!getIntent().getExtras().getBoolean(IS_PLAY)) {//new
                playMusic();
            }else{ //TH from main vs notic >> continue
            }
        }
    }

    private void updatePlayControl() {
        if (mPlayMusicService != null) {
            if (mPlayMusicService.isPlaying()) {
                playPauseImg.setImageResource(R.drawable.pb_pause);
            } else {
                playPauseImg.setImageResource(R.drawable.pb_play);
            }

            if (mPlayMusicService.isShuffle()) {
                shuffleImg.setImageResource(R.drawable.ic_widget_shuffle_on);
            } else {
                shuffleImg.setImageResource(R.drawable.ic_widget_shuffle_off);
            }

            if (mPlayMusicService.isRepeat()) {
                repeatImg.setImageResource(R.drawable.ic_widget_repeat_one);
            } else {
                repeatImg.setImageResource(R.drawable.ic_widget_repeat_off);
            }

        }
    }

    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            mPlayMusicService = bindPlayActivityToService((PlayMusicService.LocalBinder) service);
            mPlayMusicService.setRepeat(false);
            playMusic();
            updateSeekBar();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d("CHECK", "DISCONECTED");
        }
    };

    private void playMusic() {
        mPlayMusicService.playMusic(curSongPath);
        Song song = curSongList.get(currentSongPos);
        mPlayMusicService.setDataForNotification(curSongList, currentSongPos, song, song.getAlbumImagePath());
        updateTitle();
        updateAlbum();
        updateHomeActivity();

        Intent intent1 = new Intent(this, PlayMusicService.class);
        startService(intent1);
        mPlayMusicService.showNotification(true);

        updatePlayControl();

    }

    private void updateTitle() {
        songNameTv.setText(curSongList.get(currentSongPos).getTitle());
        artistNameTv.setText(curSongList.get(currentSongPos).getArtist());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateAlbum() {

        Intent intent1 = new Intent(Receivers.ACTION_CHANGE_ALBUM_ART);
        intent1.putExtra(KEY_ALBUM_PLAY, curSongList.get(currentSongPos).getAlbumImagePath());
        sendBroadcast(intent1);
        viewPagerPlayAdapter.fragmentPlayMusic.startAnnimateAlbumThumb();

        Bitmap bitmap;
        String albumPath = curSongList.get(currentSongPos).getAlbumImagePath();
        if (albumPath != null) {
            bitmap = BitmapFactory.decodeFile(albumPath);
        } else {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.default_player_bg);
        }
        bitmap = BlurBuilder.blur(this, bitmap);
        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), bitmap);
        bgViewGroup.setBackground(bitmapDrawable);
    }


    private PlayMusicService bindPlayActivityToService(PlayMusicService.LocalBinder service) {
        PlayMusicService.LocalBinder binder = service;
        PlayMusicService mPlayMusicService = binder.getInstantBoundService();
        PlayManager.getInstance().setPlayMusicService(mPlayMusicService);
        return mPlayMusicService;
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setStatusBarTranslucent(true, this);
        updateTitle();
    }

    private void getDataFromIntent() {
        Intent intent = getIntent();
        if (intent.getExtras().getBoolean(IS_PLAY)) {
            mPlayMusicService = PlayManager.getInstance().getPlayMusicService();
            curSongPath = mPlayMusicService.getCurrentSong().getPath();
            currentSongPos = mPlayMusicService.getCurrentSongPos();
            curSongList = mPlayMusicService.getPlayingSongList();
            isShuffle = mPlayMusicService.isShuffle();

        } else {
            curSongPath = intent.getExtras().getString(SONG_PATH);
            currentSongPos = intent.getExtras().getInt(SONG_POS);
            curSongList = (ArrayList<Song>) intent.getExtras().getSerializable(LIST_SONG);
        }

        songs.clear();
        songs.addAll(curSongList);
    }

    private void setupViewPage() {

        String curPlayListThumbStringPath = songs.get(currentSongPos).getAlbumImagePath();
        viewPagerPlayAdapter = new ViewPagerPlayAdapter(getSupportFragmentManager(), songs,curPlayListThumbStringPath );
        viewPager.setAdapter(viewPagerPlayAdapter);
        viewPager.setOffscreenPageLimit(2);
        viewPager.setCurrentItem(0);
    }

    private void updateSeekBar() {
        int totalTime = mPlayMusicService.getTotalTime();
        totalTimeTv.setText(Common.miliSecondToString(totalTime));
        seekbarPlay.setMax(totalTime);

        int currentLength = mPlayMusicService.getCurrentLength();
        if (!isSeeking) {
            seekbarPlay.setProgress(currentLength);
            curTimeTv.setText(Common.miliSecondToString(currentLength));
        }

        Handler musicHander = new Handler();
        musicHander.post(new Runnable() {
            @Override
            public void run() {
                updateSeekBar();
            }
        });

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unRegisterBroadcastSongComplete();
//        unRegisterBroadcastSwitchSong();
        PlayManager.getInstance().setPlaySongActivity(null);
        //

    }

    BroadcastReceiver completeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            nextPlayer();
            updateSeekBar();
            mPlayMusicService.showNotification(true);
//                updateHomeActivity();
//                Common.updateMainActivity();
        }
    };

    BroadcastReceiver broadcastReceiverSwitchSong = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            currentSongPos = intent.getExtras().getInt(PlayMusicService.KEY_ID_SWITH);
            curSongPath = curSongList.get(currentSongPos).getPath();
            mPlayMusicService.setDataForNotification(curSongList, currentSongPos,
                    curSongList.get(currentSongPos), curSongList.get(currentSongPos).getAlbumImagePath());
            playMusic();
            mPlayMusicService.showNotification(true);
        }
    };

    private void registerBroadcastSwitchSong() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ACTION_SWITCH_SONG);
        registerReceiver(broadcastReceiverSwitchSong, intentFilter);
    }

    private void registerBroadcastSongComplete() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(PlayMusicService.ACTION_COMPLETE_SONG);

        registerReceiver(completeReceiver, intentFilter);
    }

    private void unRegisterBroadcastSongComplete() {
        unregisterReceiver(completeReceiver);
    }

    private void unRegisterBroadcastSwitchSong() {
        unregisterReceiver(broadcastReceiverSwitchSong);
    }

    private void updateHomeActivity() {
        Intent intent = new Intent(ACTION_UPDATE_PlAY_STATUS);
        sendBroadcast(intent);
    }

}

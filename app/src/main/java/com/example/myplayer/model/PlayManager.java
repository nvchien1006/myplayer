package com.example.myplayer.model;

import com.example.myplayer.view.activity.MainActivity;
import com.example.myplayer.view.activity.PlaySongActivity;
import com.example.myplayer.view.services.PlayMusicService;

public class PlayManager {

    private static PlayManager mPlayManager;
    private PlayMusicService playMusicService;
    private PlaySongActivity playSongActivity;
    private MainActivity mainActivity;

    public PlayManager() {
    }

    public static PlayManager getInstance() {
        if (mPlayManager == null) {
            mPlayManager = new PlayManager();
        }
        return mPlayManager;
    }

    public PlayMusicService getPlayMusicService() {
        return playMusicService;
    }

    public void setPlayMusicService(PlayMusicService playMusicService) {
        this.playMusicService = playMusicService;
    }

    public PlaySongActivity getPlaySongActivity() {
        return playSongActivity;
    }

    public void setPlaySongActivity(PlaySongActivity playSongActivity) {
        this.playSongActivity = playSongActivity;
    }

    public MainActivity getMainActivity() {
        return mainActivity;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }
}

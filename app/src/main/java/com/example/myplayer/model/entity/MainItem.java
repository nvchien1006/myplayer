package com.example.myplayer.model.entity;

public class MainItem {
    private int iconId;
    private String title;
    private int songNumbers;

    public MainItem(int iconId, String title, int songNumbers) {
        this.iconId = iconId;
        this.title = title;
        this.songNumbers = songNumbers;
    }

    public int getIconId() {
        return iconId;
    }

    public void setIconId(int iconId) {
        this.iconId = iconId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSongNumbers() {
        return songNumbers;
    }

    public void setSongNumbers(int songNumbers) {
        this.songNumbers = songNumbers;
    }
}

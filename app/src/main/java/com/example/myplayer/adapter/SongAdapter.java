package com.example.myplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myplayer.R;
import com.example.myplayer.model.entity.Song;
import com.example.myplayer.view.activity.PlaySongActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.ViewHolder> {

    //
    private Context context;
    private ArrayList<Song> songs;
    private ClickItemRcv clickItemRcv;

    //
    public interface ClickItemRcv {
        void onClickItemRcv(int posItem);
    }

    public SongAdapter(Context context, ArrayList<Song> songs, ClickItemRcv clickItemRcv) {
        this.context = context;
        this.clickItemRcv = clickItemRcv;
        this.songs = songs;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_song, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Song item = songs.get(i);
        viewHolder.titleTv.setText(item.getTitle() + "");
        viewHolder.artist.setText(item.getArtist() + "");

    }

    @Override
    public int getItemCount() {
        return songs.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_item_song_title)
        TextView titleTv;
        @BindView(R.id.tv_item_song_artist)
        TextView artist;

        //2
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //3
            itemView.setOnClickListener(this);

        }

        //5
        @Override
        public void onClick(View v) {
            try {

                int po = getAdapterPosition();
                if (clickItemRcv != null) {
                    PlaySongActivity.openPlayActivity(context,po,false,songs);
                    clickItemRcv.onClickItemRcv(po);
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }


}

package com.example.myplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myplayer.R;
import com.example.myplayer.model.entity.MainItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    //
    private Context context;
    private List<MainItem> mainList;
    private ClickItemRcv clickItemRcv;

    //
    public interface ClickItemRcv {
        void onClickItemRcv(int posItem);
    }

    public CategoryAdapter(Context context, List<MainItem> mainList, ClickItemRcv clickItemRcv) {
        this.context = context;
        this.clickItemRcv = clickItemRcv;
        this.mainList = mainList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_main, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        MainItem item = mainList.get(i);
        Glide.with(context).load(item.getIconId()).into(viewHolder.thumbImg);
        viewHolder.titleTv.setText(item.getTitle() + "");
        viewHolder.countTv.setText(item.getSongNumbers() + "");

    }

    @Override
    public int getItemCount() {
        return mainList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.img_item)
         ImageView thumbImg;
        @BindView(R.id.txt_item_title)
         TextView titleTv;
        @BindView(R.id.txt_item_num)
         TextView countTv;

        //2
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //3
            itemView.setOnClickListener(this);

        }

        //5
        @Override
        public void onClick(View v) {
            try {

                int po = getAdapterPosition();
                if (clickItemRcv != null) {
                    clickItemRcv.onClickItemRcv(po);
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }
}



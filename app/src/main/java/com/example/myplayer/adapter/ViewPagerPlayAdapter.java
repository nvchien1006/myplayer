package com.example.myplayer.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.myplayer.model.entity.Song;
import com.example.myplayer.view.fragment.FragmentPlayList;
import com.example.myplayer.view.fragment.FragmentPlayMusic;

import java.util.ArrayList;

public class ViewPagerPlayAdapter extends FragmentStatePagerAdapter {

    public ArrayList<Song> songList;
    public String coverPath;

    public FragmentPlayList fragmentPlayList;
    public FragmentPlayMusic fragmentPlayMusic;


    public ViewPagerPlayAdapter(FragmentManager fm, ArrayList<Song> songList, String coverPath) {
        super(fm);
        this.songList = songList;
        this.coverPath = coverPath;
        fragmentPlayList =FragmentPlayList.newInstance(songList);
        fragmentPlayMusic = FragmentPlayMusic.newInstance(coverPath);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return fragmentPlayMusic;
            case 1:
                return fragmentPlayList;
        }
        return fragmentPlayMusic;
    }

    @Override
    public int getCount() {
        return 2;
    }

}

package com.example.myplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myplayer.R;
import com.example.myplayer.model.entity.Artist;
import com.example.myplayer.model.entity.Song;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.ViewHolder> {

    //
    private Context context;
    private List<Artist> artists;
    private ClickItemRcv clickItemRcv;

    //
    public interface ClickItemRcv {
        void onClickItemRcv(int posItem);
    }

    public ArtistAdapter(Context context, List<Artist> artists, ClickItemRcv clickItemRcv) {
        this.context = context;
        this.clickItemRcv = clickItemRcv;
        this.artists = artists;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_artist, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Artist item = artists.get(i);
        viewHolder.artist.setText(item.getName() + "");

    }

    @Override
    public int getItemCount() {
        return artists.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.artist_title_item)
        TextView artist;

        //2
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            //3
            itemView.setOnClickListener(this);

        }

        //5
        @Override
        public void onClick(View v) {
            try {

                int po = getAdapterPosition();
                if (clickItemRcv != null) {
                    clickItemRcv.onClickItemRcv(po);
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }
}

package com.example.myplayer.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.myplayer.R;
import com.example.myplayer.model.entity.Album;
import com.example.myplayer.model.entity.Artist;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumAdapter extends RecyclerView.Adapter<AlbumAdapter.ViewHolder> {

    //
    private Context context;
    private List<Album> albums;
    private ClickItemRcv clickItemRcv;

    //
    public interface ClickItemRcv {
        void onClickItemRcv(int posItem);
    }

    public AlbumAdapter(Context context, List<Album> albums, ClickItemRcv clickItemRcv) {
        this.context = context;
        this.clickItemRcv = clickItemRcv;
        this.albums = albums;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_album_grid, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Album album = albums.get(i);
        viewHolder.title.setText(album.getTitle() + "");
        viewHolder.artist.setText(album.getArtist() + "");
        String thumbnailAlbum = album.getAlbumArtPath();
        if(thumbnailAlbum != null) {
            Glide.with(context).load(thumbnailAlbum).into(viewHolder.thumb);
        }else{
            viewHolder.thumb.setImageResource(R.drawable.default_cover_big);
        }

    }

    @Override
    public int getItemCount() {
        return albums.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @BindView(R.id.tv_item_album_title)
        TextView title;
        @BindView(R.id.tv_item_artist_album)
        TextView artist;
        @BindView(R.id.iv_item_album_img)
        ImageView thumb;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            try {

                int po = getAdapterPosition();
                if (clickItemRcv != null) {
                    clickItemRcv.onClickItemRcv(po);
                }

            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }
    }
}
